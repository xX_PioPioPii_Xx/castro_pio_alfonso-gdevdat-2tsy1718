﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;


class Program
{
    public static string connectionString = "Data Source=TAFT-CL540;Initial Catalog=CastroPioAlfonsoTG001;User ID=sa;Password=benilde";

    static void Main()
    {

        TryCreateTable();
        while (true)
        {
            Console.WriteLine("INPUT TYPE: Number, Username, Email or SELECT:");
            string[] input = Console.ReadLine().Split(',');
            try
            {
                char c = char.ToLower(input[0][0]);
                if (c == 's')
                {
                    DisplayDogs();
                    continue;
                }
                int number = int.Parse(input[0]);
                string username = input[1];
                string email = input[2];
                AddUser(number, username, email);
            }
            catch
            {
                Console.WriteLine("Input error");
            }
        }
    }

    static void TryCreateTable()
    {
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            con.Open();
            try
            {
                using (SqlCommand command = new SqlCommand("CREATE TABLE User1 (Number INT, Username TEXT, Email TEXT)", con))
                {
                    command.ExecuteNonQuery();
                }
            }
            catch
            {
                Console.WriteLine("Table not created.");
            }
        }
    }

    static void AddUser(int number, string username, string email)
    {
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            con.Open();
            try
            {
                using (SqlCommand command = new SqlCommand("INSERT INTO User1 VALUES(@Number, @Username, @Email)", con))
                {
                    command.Parameters.Add(new SqlParameter("Number", number));
                    command.Parameters.Add(new SqlParameter("Username", username));
                    command.Parameters.Add(new SqlParameter("Email", email));
                    command.ExecuteNonQuery();
                }
            }
            catch
            {
                Console.WriteLine("Count not insert.");
            }
        }
    }

    static void DisplayDogs()
    {
        List<User> users = new List<User>();
        using (SqlConnection con = new SqlConnection(connectionString))
        {
            con.Open();

            using (SqlCommand command = new SqlCommand("SELECT * FROM User1", con))
            {
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    int number = reader.GetInt32(0);    
                    string username = reader.GetString(1);  
                    string email = reader.GetString(2); 
                    users.Add(new User() { Number = number, Username = username, Email = email });
                }
            }
        }
        foreach (User user in users)
        {
            Console.WriteLine(user);
        }
    }
}


public class User
{
    public int Number { get; set; }
    public string Username { get; set; }
    public string Email { get; set; }
    public override string ToString()
    {
        return string.Format("Number: {0}, Username: {1}, Email: {2}", Number, Username, Email);
    }
}